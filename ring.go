package stats

import (
	"fmt"
)

type Ring[T Ordered] struct {
	size   int
	i      int
	values []T
}

func NewRing[T Ordered](size int) (*Ring[T], error) {
	if size <= 0 {
		return nil, fmt.Errorf("size must be > 0")
	}

	r := Ring[T]{
		size:   size,
		values: make([]T, size),
	}
	return &r, nil
}

func (r *Ring[T]) Push(v T) {
	r.values[r.i] = v
	r.i = (r.i + 1) % r.size
}

func (r *Ring[T]) Avg() float64 {
	m, err := Mean(r.values)
	if err != nil {
		panic(err)
	}
	return m
}

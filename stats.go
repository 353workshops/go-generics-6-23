package stats

import (
	"fmt"
	"math"
)

//type Value int // define new type
// type Value = int // type alias

type Ordered interface {
	~int | ~float64 // | string
	// | time.Time // Not possible
}

// type byte = uint8
// type rune = int32

// int, int8, int16, int32, int64
// uint, uint8, uint16, uint32, uint64
// float32, float64
// complex64, complex128
// math/big

// "2021-03-14T07:46:55"

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

// Exercise: Write generics Average/Mean that will always return float64
func Mean[T Ordered](values []T) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("mean of empty slice")
	}

	sum := values[0]
	for _, v := range values[1:] {
		sum += v
	}
	return float64(sum) / float64(len(values)), nil
}

// Exercise: Write
// Option 1: return slice size len(values) where first window are math.NaN()
// Option 2: return slice size if len(values)-window+1
func MovingAvg[T Ordered](values []T, window int) ([]float64, error) {
	if len(values) < window {
		return nil, fmt.Errorf("slice len must be >= window")
	}

	r, err := NewRing[T](window)
	if err != nil {
		return nil, err
	}

	out := make([]float64, len(values))
	for i := 0; i < window-1; i++ {
		r.Push(values[i])
		out[i] = math.NaN()
	}

	for i, v := range values[window-1:] {
		r.Push(v)
		out[i+window-1] = r.Avg()
	}

	return out, nil
}

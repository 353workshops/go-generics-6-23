package stats

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPush(t *testing.T) {
	r, err := NewRing[int](3)
	require.NoError(t, err)

	for i := 0; i < 10; i++ {
		r.Push(i)
	}

	require.Equal(t, []int{9, 7, 8}, r.values)
}

func TestAvg(t *testing.T) {
	r, err := NewRing[int](3)
	require.NoError(t, err)
	r.Push(1)
	r.Push(2)
	r.Push(4)
	require.InDelta(t, 2.333, r.Avg(), 0.01)
}

package stats

import (
	"encoding/json"
)

type Serializable interface {
	~int | ~float64 | ~string
}

type Ptr[T Serializable] interface {
	*T
}

// func Decode[T Serializable, P Ptr[T]](data []byte, v P) error {
func Decode[T Serializable](data []byte, v *T) error {
	return json.Unmarshal(data, v)
}

/* Does not work ATM
type User struct {
	ID   string
	Name string
}

type Group struct {
	ID      string
	Members []string
}

func PrintValue[T User | Group](v T) {
	fmt.Println(v.ID)
}
*/

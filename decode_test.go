package stats

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDecode(t *testing.T) {
	data := []byte("7")
	var n int
	err := Decode(data, &n)
	require.NoError(t, err)
	require.Equal(t, 7, n)
}

package stats

import (
	"math"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMax(t *testing.T) {
	values := []int{3, 1, 2}
	m, err := Max(values)
	require.NoError(t, err)
	require.Equal(t, 3, m)

	fvalues := []float64{3, 1, 2}
	fm, err := Max(fvalues)
	require.NoError(t, err)
	require.Equal(t, 3.0, fm)

	mvalues := []time.Month{time.January, time.December, time.October}
	mm, err := Max(mvalues)
	require.NoError(t, err)
	require.Equal(t, time.December, mm)

}

func TestMaxEmpty(t *testing.T) {
	_, err := Max[int](nil)
	require.Error(t, err)
}

func TestMovingAvg(t *testing.T) {
	v := []int{1, 1, 2, 3, 5, 8}
	expected := []float64{math.NaN(), 1.0, 1.5, 2.5, 4.0, 6.5}

	out, err := MovingAvg(v, 2)
	require.NoError(t, err)
	require.InDeltaSlice(t, expected, out, 0.001)
}

// func TestMean(t *testing.T) {
// 	values := []int{4, 2, 4}
// 	m, err := Mean(values)
// 	require.NoError(t, err)
// 	require.InDelta(t, 3.3333, m, 0.001)
// }

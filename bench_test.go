package stats

import "testing"

func Sum[T Ordered](values []T) T {
	if len(values) == 0 {
		return 0
	}

	s := values[0]
	for _, v := range values[1:] {
		s += v
	}
	return s
}

func SumAny(values []any) any {
	if len(values) == 0 {
		return 0
	}

	s := values[0].(int)
	for _, v := range values[1:] {
		s += v.(int)
	}
	return s
}

var values []int
var ivalues []any
var s int

func init() {
	for i := 0; i < 10_000; i++ {
		values = append(values, i)
		ivalues = append(ivalues, i)
	}

}

func BenchmarkSum(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s = Sum(values)
	}
}

func BenchmarkSumAny(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s = SumAny(ivalues).(int)
	}
}
